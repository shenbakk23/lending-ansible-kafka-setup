kafka
=========

OS: ubuntu 16.04
Desc: Install and setup kafka cluster
Requirement: EBS optimized instance should be used

# Mandatory configuration at groupvars

1. Zookeeper security group should pe mentioned 
   Ex:- zookeeper_group: "security_group_prod_sg_billpayments_rechargezookeeper"

# Confuration templates

1. Default value of server,consumer & producer has been given , if we want to update values then we have to  mentioned it on groupvar so that it will automatically replace defaults.

   Ex:- # kafka server configuration
        kafka_num_io_threads: 8

        # kafka consumer configuration
        consumer_heartbeat_interval_ms: 8000

        # kafka producer configuration
        producer_connections_max_idle_ms: 900000 

# Others 

 -  Default block device mapping on AWS HVM is /dev/xvd[a-z], if we are using Nitro-based instances. The device names are /dev/nvme[0-26]n1 , change default value as per your needs.

 - config dir: /opt/kafka
   data dir: /data/kafka/data
   logs dir: /data/kafka/logs 
